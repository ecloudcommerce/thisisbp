<?php

class Best4Mage_ConfigurableProductsSimplePrices_Model_Observer
{
	public function showOutOfStock($observer){
        if($_product = Mage::registry('current_product')){
			if(!$_product->getIsSalable()) return $this;
			$_cpsp = Mage::helper('configurableproductssimpleprices');
			if($_cpsp->isEnable($_product) && (1*$_cpsp->showCpspStock($_product) !== 0)){
				Mage::helper('catalog/product')->setSkipSaleableCheck(true);
			}
		}
    }

    public function filterConfigurableSimple($observer)
    {
    	return $this; // return to show out of stock product.
    	if($_product = Mage::registry('current_product'))
    	{	
    		$block = $observer->getBlock();
    		
    		if(!$block instanceof Mage_Catalog_Block_Product_View_Type_Configurable )
    		{
    			return $this;	
    		}

			if(!$_product->getIsSalable()) return $this;
			$_cpsp = Mage::helper('configurableproductssimpleprices'); 
			if($_cpsp->isEnable($_product) && (1*$_cpsp->showCpspStock($_product) !== 0))
			{	
				$subProducts = $block->getAllowProducts();

				if(count($subProducts)){
					$finalSubProducts = array();
					foreach ($subProducts as $value)
					{
						if($value->getIsSalable())
						{
							$finalSubProducts[] = $value;
						}
					}
					$block->setAllowProducts($finalSubProducts);
				}
			}
		}	
    }

    public function setQuoteItemOptions($observer) {
    	$options = array();
        $_item = $observer->getQuoteItem();
        $_helper = Mage::helper('configurableproductssimpleprices');
        $_product = $_item->getProduct();
        $_buyReq = $_item->getBuyRequest();
    	if($_product->getTypeId() != 'configurable') return $this;
        if(!$_helper->isEnable($_product)) return $this;

        $_product->setCpspOptionPrice(0);
        $_simple = Mage::getSingleton('catalog/product_type_configurable')->getProductByAttributes($_buyReq->getSuperAttribute(),$_product);
        if ($_simple->getHasOptions()) {
            foreach ($_simple->getProductOptionsCollection() as $option) {
                $option->setProduct($_simple);
                $_simple->addOption($option);
            }
        }
    	$_postOptions = $_buyReq->getOptions();
        if(isset($_postOptions) && $_simple)
        {
        	if($_simProOptions = $_helper->getCustomOptions($_simple, $_buyReq))
            {
				if(count($_simProOptions) > 0)
                {
                    foreach ($_simProOptions as $op)
                    {
						if($op['value'] != null)
                        {
							$options[] = $op;
                            $_product->setCpspOptionPrice($_product->getCpspOptionPrice()+$op['price']);
						}
					}
				}
			}
    		$_item->addOption(array(
                'code' => 'additional_options',
                'value' => serialize($options),
                'product_id' => $_product->getId()
            ));
    	}
    }

    public function quoteSetProductToOrderItem($observer)
    {
        $orderItem = $observer->getOrderItem();
        $item = $observer->getItem();
        if($additionalOption = $item->getOptionByCode('additional_options')){
            $options = $orderItem->getProductOptions();
            if(!is_array($options)){
                $options = array($options);
            }
            $options[$additionalOption->getCode()] = unserialize($additionalOption->getValue());
            $orderItem->setProductOptions($options);
        }
        
    }
}
