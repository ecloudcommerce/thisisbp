var bp = {
    xsmall: 479,
    small: 599,
    medium: 770,
    large: 979,
    xlarge: 1199
};
var PointerManager = {
    MOUSE_POINTER_TYPE: 'mouse',
    TOUCH_POINTER_TYPE: 'touch',
    POINTER_EVENT_TIMEOUT_MS: 500,
    standardTouch: false,
    touchDetectionEvent: null,
    lastTouchType: null,
    pointerTimeout: null,
    pointerEventLock: false,

    getPointerEventsSupported: function() {
        return this.standardTouch;
    },

    getPointerEventsInputTypes: function() {
        if (window.navigator.pointerEnabled) { //IE 11+
            //return string values from http://msdn.microsoft.com/en-us/library/windows/apps/hh466130.aspx
            return {
                MOUSE: 'mouse',
                TOUCH: 'touch',
                PEN: 'pen'
            };
        } else if (window.navigator.msPointerEnabled) { //IE 10
            //return numeric values from http://msdn.microsoft.com/en-us/library/windows/apps/hh466130.aspx
            return {
                MOUSE:  0x00000004,
                TOUCH:  0x00000002,
                PEN:    0x00000003
            };
        } else { //other browsers don't support pointer events
            return {}; //return empty object
        }
    },

    /**
     * If called before init(), get best guess of input pointer type
     * using Modernizr test.
     * If called after init(), get current pointer in use.
     */
    getPointer: function() {
        // On iOS devices, always default to touch, as this.lastTouchType will intermittently return 'mouse' if
        // multiple touches are triggered in rapid succession in Safari on iOS
        if(Modernizr.ios) {
            return this.TOUCH_POINTER_TYPE;
        }

        if(this.lastTouchType) {
            return this.lastTouchType;
        }

        return Modernizr.touch ? this.TOUCH_POINTER_TYPE : this.MOUSE_POINTER_TYPE;
    },

    setPointerEventLock: function() {
        this.pointerEventLock = true;
    },
    clearPointerEventLock: function() {
        this.pointerEventLock = false;
    },
    setPointerEventLockTimeout: function() {
        var that = this;

        if(this.pointerTimeout) {
            clearTimeout(this.pointerTimeout);
        }

        this.setPointerEventLock();
        this.pointerTimeout = setTimeout(function() { that.clearPointerEventLock(); }, this.POINTER_EVENT_TIMEOUT_MS);
    },

    triggerMouseEvent: function(originalEvent) {
        if(this.lastTouchType == this.MOUSE_POINTER_TYPE) {
            return; //prevent duplicate events
        }

        this.lastTouchType = this.MOUSE_POINTER_TYPE;
        jQuery(window).trigger('mouse-detected', originalEvent);
    },
    triggerTouchEvent: function(originalEvent) {
        if(this.lastTouchType == this.TOUCH_POINTER_TYPE) {
            return; //prevent duplicate events
        }

        this.lastTouchType = this.TOUCH_POINTER_TYPE;
        jQuery(window).trigger('touch-detected', originalEvent);
    },

    initEnv: function() {
        if (window.navigator.pointerEnabled) {
            this.standardTouch = true;
            this.touchDetectionEvent = 'pointermove';
        } else if (window.navigator.msPointerEnabled) {
            this.standardTouch = true;
            this.touchDetectionEvent = 'MSPointerMove';
        } else {
            this.touchDetectionEvent = 'touchstart';
        }
    },

    wirePointerDetection: function() {
        var that = this;

        if(this.standardTouch) { //standard-based touch events. Wire only one event.
            //detect pointer event
            jQuery(window).on(this.touchDetectionEvent, function(e) {
                switch(e.originalEvent.pointerType) {
                    case that.getPointerEventsInputTypes().MOUSE:
                        that.triggerMouseEvent(e);
                        break;
                    case that.getPointerEventsInputTypes().TOUCH:
                    case that.getPointerEventsInputTypes().PEN:
                        // intentionally group pen and touch together
                        that.triggerTouchEvent(e);
                        break;
                }
            });
        } else { //non-standard touch events. Wire touch and mouse competing events.
            //detect first touch
            jQuery(window).on(this.touchDetectionEvent, function(e) {
                if(that.pointerEventLock) {
                    return;
                }

                that.setPointerEventLockTimeout();
                that.triggerTouchEvent(e);
            });

            //detect mouse usage
            jQuery(document).on('mouseover', function(e) {
                if(that.pointerEventLock) {
                    return;
                }

                that.setPointerEventLockTimeout();
                that.triggerMouseEvent(e);
            });
        }
    },

    init: function() {
        this.initEnv();
        this.wirePointerDetection();
    }
};
var Instagram ={
    init:function(){
        if (jQuery('body').hasClass('cms-home')) {
            jQuery('#instafeed').html('');
            var userFeed = new Instafeed({
                get: 'user', //Datos de thisisbp comentados.
                clientId: 'd5d494696f5046188429e8fdc7e5bdd8', //389e36ef15f9418f92618cc831522ab3
                userId: '401397342', //414480605
                accessToken: '401397342.d5d4946.45bf89d7b418441591770e71083fef32',//414480605.389e36e.63f6ae361412474d9a6fb6ce4d452b7f
                template: '<li class="instagram_item span3"/><a target="_blank" href="{{link}}"><img src="{{image}}"/></a></li>',
                limit:4,
                resolution: 'standard_resolution'
            });
            userFeed.run();
        }
    }
}

var ProductMediaManager = {
    swapImageDetail : function(base_image,zoom_image,thumb_image){
        if(jQuery('.col-main-details').hasClass('col-main-details-style_2')){
            ConfigurableMediaImages.checkImageLoaded(base_image,function(image){
                jQuery('.product-view .product-img-box-top .arw-fancybox').last().attr('href',image).find('img').attr('src',image);
            });
        }else{
            ConfigurableMediaImages.checkImageLoaded(thumb_image,function(image){
                jQuery('.product-view #arw-zoom img').attr('src',thumb_image[0].src);
            });
            ConfigurableMediaImages.checkImageLoaded(zoom_image,function(image){
                if(!jQuery('body').hasClass('arexworks-quickview-index')){
                    try{
                        jQuery('.product-view #arw-zoom').data('zoom').destroy();
                    }catch (ex){}
                    jQuery('.product-view #arw-zoom').attr('href',zoom_image[0].src);
                    try{
                        jQuery('.product-view #arw-zoom').CloudZoom();
                    }catch (ex){}
                }
            });
            if(jQuery('.zoom-fancybox-button').parent().length > 0){
                var flag = true;
                jQuery('.zoom-fancybox-button').each(function(){
                    if(jQuery(this).data('image') == thumb_image[0].src){
                        flag = false;
                    }
                })
                if(flag){
                    var $parent_popup = jQuery('.zoom-fancybox-button').parent();
                    var $popup_object = jQuery('<a/>');
                    $popup_object.addClass('zoom-fancybox-button')
                        .attr('rel','gallery')
                        .attr('href',base_image[0].src)
                        .data('image',thumb_image[0].src);
                    $parent_popup.append($popup_object);
                }
            }
            jQuery('#arw-zoom #image').height()
        }
    },
    destroyZoom: function() {
        jQuery('.zoomContainer').remove();
        jQuery('.product-image-gallery .gallery-image').removeData('elevateZoom');
    },
    createZoom: function(image) {
        // Destroy since zoom shouldn't be enabled under certain conditions
        ProductMediaManager.destroyZoom();

        if(
            // Don't use zoom on devices where touch has been used
            PointerManager.getPointer() == PointerManager.TOUCH_POINTER_TYPE
            // Don't use zoom when screen is small, or else zoom window shows outside body
            || Modernizr.mq("screen and (max-width:" + bp.medium + "px)")
        ) {
            return; // zoom not enabled
        }

        if(image.length <= 0) { //no image found
            return;
        }

        if(image[0].naturalWidth && image[0].naturalHeight) {
            var widthDiff = image[0].naturalWidth - image.width() - ProductMediaManager.IMAGE_ZOOM_THRESHOLD;
            var heightDiff = image[0].naturalHeight - image.height() - ProductMediaManager.IMAGE_ZOOM_THRESHOLD;

            if(widthDiff < 0 && heightDiff < 0) {
                //image not big enough

                image.parents('.product-image').removeClass('zoom-available');

                return;
            } else {
                image.parents('.product-image').addClass('zoom-available');
            }
        }

        //image.elevateZoom();
    },
    swapImage: function(targetImage) {
        targetImage = jQuery(targetImage);
        targetImage.addClass('gallery-image');

        ProductMediaManager.destroyZoom();

        var imageGallery = jQuery('.product-image-gallery');

        if(targetImage[0].complete) { //image already loaded -- swap immediately

            imageGallery.find('.gallery-image').removeClass('visible');

            //move target image to correct place, in case it's necessary
            imageGallery.append(targetImage);

            //reveal new image
            targetImage.addClass('visible');

            //wire zoom on new image
            ProductMediaManager.createZoom(targetImage);

        } else { //need to wait for image to load

            //add spinner
            imageGallery.addClass('loading');

            //move target image to correct place, in case it's necessary
            imageGallery.append(targetImage);

            //wait until image is loaded
            imagesLoaded(targetImage, function() {
                //remove spinner
                imageGallery.removeClass('loading');

                //hide old image
                imageGallery.find('.gallery-image').removeClass('visible');

                //reveal new image
                targetImage.addClass('visible');

                //wire zoom on new image
                ProductMediaManager.createZoom(targetImage);
            });

        }
    },
    wireThumbnails: function() {
        //trigger image change event on thumbnail click
        jQuery('.product-image-thumbs .thumb-link').click(function(e) {
            e.preventDefault();
            var jlink = jQuery(this);
            var target = jQuery('#image-' + jlink.data('image-index'));

            ProductMediaManager.swapImage(target);
        });
    },
    swapImageList : function(list_image , $product){
        ConfigurableMediaImages.checkImageLoaded(list_image,function(image){
            jQuery('img.product-collection-image',$product).attr('src',image);
        })
    },
    init: function() {
        jQuery(document).trigger('product-media-loaded', ProductMediaManager);
    }
};
jQuery(document).ready(function(){
    PointerManager.init();
    ProductMediaManager.init();
    Instagram.init();
});