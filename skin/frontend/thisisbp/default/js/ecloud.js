jQuery(document).ready(function(){
 	//Theme.init();
 	CatalogView.init();
 	//ProductPage.init();
 	HEADER.init();
});
var HEADER = {
	init: function(){
		this.menu();
	},

	menu: function(){
	    if (jQuery(window).width() < 991){
			jQuery( '.header-top-link-ul span' ).click(function() {
			  jQuery('.links').toggle();
			});
		}
	},
}
var CatalogView = {
	init: function(){
		if (jQuery('.catalog-category-view').length || jQuery('.catalogsearch-result-index').length) {
			this.onInfiniteScrollReady();
			//this.toggleSwatches();
			this.InifiniteScrollRendered();
			this.swatchUnico();
			//this.onAjaxLayeredFilter();
		}
	},
	// onAjaxLayeredFilter: function(){
	// 	jQuery('.col-main').bind('DOMSubtreeModified', function() {
	// 		// jQuery(document).trigger('ajaxFilteredContent');
	// 		setTimeout(function(){
	// 			jQuery('.ajax_load').fadeOut();
	// 		},300);
	// 	});
	// },
	onInfiniteScrollReady: function(){
		jQuery(document).on('ajaxStop',function(){
			jQuery(window).on('infiniteScrollReady', function(items){
				console.log('infiniteScrollReady document');
				ConfigurableMediaImages.ajaxLoadSwatchList();
			});
		});
	},

	swatchUnico: function(){
			jQuery('.products-grid li.item').each(function(){
			    if(jQuery(this).find('.configurable-swatch-list').find('li').length == 1){
			        jQuery(this).find('.configurable-swatch-list').find('li').hide()
			    } 
			});	
	},
	// toggleSwatches: function(prodgrid){
	// 	jQuery(document).on('ajaxStop',function(){
	// 	    jQuery('.products-grid li').not('.toggled').each(function(){
	// 	    	if (jQuery(this).find('.configurable-swatch-list').length > 0) {
	// 		    	jQuery(this).find('.swatches-toggle').click(function(){
	// 			        jQuery(this).toggleClass('opened');
	// 			        jQuery(this).siblings('.configurable-swatch-list').slideToggle();
	// 			    });
	// 			    jQuery(this).addClass('toggled');
	// 	    	}
	// 	    });
	//     });
	// },
	InifiniteScrollRendered: function(){
		jQuery(document).on('ajaxStop',function(){
			//Integracion con infiniteScroll.
			window.ias.on('rendered', function(items){
				console.log("InifiniteScrollRendered");
				ConfigurableMediaImages.ajaxLoadSwatchList();
				CatalogView.swatchUnico();
				// Theme.toggleSwatchesOnProdList();
			});
		});
	}
}