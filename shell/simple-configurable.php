<?php
require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$file_handle = fopen("skus.csv", "r");
while (!feof($file_handle) ) {
	$line_of_text = fgetcsv($file_handle, 1024);
	$simpleSku = $line_of_text[0];
	$configurableSku = $line_of_text[1];
	$simpleProduct = Mage::getModel('catalog/product')->loadByAttribute('sku',$simpleSku);
	$configurableProduct = Mage::getModel('catalog/product')->loadByAttribute('sku',$configurableSku);
	if (false !== $configurableProduct) {
		if (false !== $simpleProduct) {
			$simpleId = $simpleProduct->getId();
			$ids = $configurableProduct->getTypeInstance()->getUsedProductIds();
			$newids = array();
			foreach ( $ids as $id ) {
				$newids[$id] = 1;
			}
			$newids[$simpleId] = 1;
			echo "Actualizando configurables " . $configurableSku;
			echo "<br>";
			Mage::getResourceModel('catalog/product_type_configurable')->saveProducts($configurableProduct, array_keys($newids));
		}else{
			echo $simpleSku.' no existe';	
		}
	}else{
		echo $configurableSku.' no existe';
	}
}
fclose($file_handle);
?>